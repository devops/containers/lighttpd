FROM debian:bookworm

RUN apt -y update && \
    apt -y install lighttpd && \
    rm -rf /var/lib/apt/lists/*

RUN sed -i \
    -e 's|^server\.errorlog .*|server.errorlog = "/proc/self/fd/2"|' \
    -e 's|^server\.pid-file .*|server.pid-file = "/tmp/lighttpd.pid"|' \
    -e 's|^server\.port .*|server.port = 8080|' \
    /etc/lighttpd/lighttpd.conf

EXPOSE 8080

CMD [ "lighttpd", "-D", "-f", "/etc/lighttpd/lighttpd.conf" ]

USER www-data
